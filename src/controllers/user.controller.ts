import { Request, Response, NextFunction } from "express";
import { getRepository } from "typeorm";
import { User } from "../entities";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";


export const create = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { name, email, password, isAdm } = req.body;
  try {
    const userRepository = getRepository(User);

    const find = await userRepository.findOne({ where: { email: email } });
    if (find) {
      res.status(400).send({ message: "E-mail already registered" });
    } else {
      const user = userRepository.create({ name, email, password, isAdm });
      await userRepository.save(user);
      res.status(201).send(user);
    }
  } catch (err) {
    next(err);
  }
};

export const login = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { email, password } = req.body;
  const userRepository = getRepository(User);

  const user = await userRepository.findOne({ where: { email: email } });

  if (user) {
    bcrypt.compare(password, user?.password as string, (err, result) => {
      if (result) {
        const token = jwt.sign(
          { id: user?.id, isAdm: user?.isAdm },
          process.env.SECRET as string,
          {
            expiresIn: "24h",
          }
        );
        res.send({ token: token });
      } else {
        res.status(401).send({
          message: "Wrong email/password",
        });
      }
    });
  } else {
    res.status(401).send({
      message: "Wrong email/password",
    });
  }
};

export const listUsers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const jwt = require("jsonwebtoken");
  const token = req.headers.authorization?.split(" ")[1];
  const decoded = await jwt.decode(token as string);
  const { userId, isAdm } = decoded;

  if (isAdm) {
    const userRepository = getRepository(User);
    const users = await userRepository.find();
    res.send(users);
  } else {
    res.status(401).send({
      message: "Unauthorized",
    });
  }
};

export const listCurrentUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const jwt = require("jsonwebtoken");
  const token = req.headers.authorization?.split(" ")[1];
  const decoded = await jwt.decode(token as string);
  const { id, isAdm } = decoded;
  const userRepository = getRepository(User);
  const user = await userRepository.findOne({ where: { id: id } });

  res.send(user);
};

export const update = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.body;
  const { uuid } = req.params;
  const jwt = require("jsonwebtoken");
  const token = req.headers.authorization?.split(" ")[1];
  const decoded = await jwt.decode(token as string);
  const { id, isAdm } = decoded;
  const userRepository = getRepository(User);
  const keys = Object.keys(data);
  const datestemp = new Date();

  if (keys.includes("isAdm")) {
    res
      .status(401)
      .send({ message: "it is not allowed to change the value of isAdm" });
  }
  if (isAdm) {
    await userRepository.update(
      { id: uuid },
      { ...data, updatedOn: datestemp }
    );
    const user = await userRepository.findOne({ where: { id: uuid } });
    res.send(user);
  } else if (!isAdm && id === uuid) {
    await userRepository.update(
      { id: uuid },
      { ...data, updatedOn: datestemp }
    );
    const user = await userRepository.findOne({ where: { id: uuid } });
    res.send(user);
  } else {
    res.status(401).send({
      message: "Missing admin permissions",
    });
  }
};

export const deleteUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { uuid } = req.params;
  const jwt = require("jsonwebtoken");
  const token = req.headers.authorization?.split(" ")[1];
  const decoded = await jwt.decode(token as string);
  const { id, isAdm } = decoded;
  const userRepository = getRepository(User);

  if (isAdm) {
    await userRepository.delete({ id: uuid });
    res.send({
      message: "User deleted with success",
    });
  } else if (!isAdm && id === uuid) {
    await userRepository.delete({ id: uuid });
    res.send({
      message: "User deleted with success",
    });
  } else {
    res.status(401).send({
      message: "Missing admin permissions",
    });
  }
};
