import "reflect-metadata"
import express from "express"
import { connectDatabase } from "./database"
import { appRoutes } from "./routes"

connectDatabase()

const app = express()

app.use(express.json())

appRoutes(app)

export default app