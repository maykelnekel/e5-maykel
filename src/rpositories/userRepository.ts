import { EntityRepository, Repository } from "typeorm";
import { User } from "../entities";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    public async findById(id: string): Promise<User | undefined>{
        const user = await this.findOne(id)
        return user
    }
}