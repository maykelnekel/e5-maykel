import { Express } from "express"
import { create, deleteUser, listCurrentUser, listUsers, login, update } from "./controllers/user.controller"
import verifyToken from "./middlewares/token.middleware"

export const appRoutes = (app: Express) => {
    app.post('/users', create)
    app.post('/login', login)
    app.get('/users', verifyToken, listUsers)
    app.get('/users/profile', verifyToken, listCurrentUser)
    app.patch('/users/:uuid', verifyToken, update)
    app.delete('/users/:uuid', verifyToken, deleteUser)
}