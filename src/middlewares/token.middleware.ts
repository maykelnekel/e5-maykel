import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken"

const verifyToken = (req: Request, res: Response, next: NextFunction) => {
    const token = req.headers.authorization?.split(' ')[1]

    jwt.verify(token as string, process.env.SECRET as string, (err, decoded)=>{
        if (err){
            return next(res.status(401).send({
                "message": "Missing authorization headers"
            }))

        } else{
            
            next()
        }
    })
}
export default verifyToken